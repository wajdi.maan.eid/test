import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import PrimeVue from 'primevue/config';
import * as Vue from 'vue' // in Vue 3
import axios from 'axios'
import VueAxios from 'vue-axios'

import store from './store'
const app=createApp(App);
const vuetify = createVuetify({
    components,
    directives,
    
  })
  app.use(vuetify);
  app.use(router);
  app.use(store);
  app.use(VueAxios, axios)
  app.use(PrimeVue);
  app.mount('#app');
