export default {
    namespaced:true,
        state () {
            return {
             logininfo:localStorage.getItem('logininfo') != null ? localStorage.getItem('logininfo'): null,
            
            }
          },
        
          
          mutations :{
            SET_LOGIN(state,data){
                state.logininfo=data
                },
                REMOVE_LOGIN(state){
                  state.logininfo=''
                }
          
          },
        
      
        actions :{
            setlogininfo({commit},data){
                localStorage.setItem('logininfo',data)
              
            },
            removelogininfo({commit}){
                localStorage.removeItem('logininfo')
             
             },
          
        },
      
     
        
          getters :{
            logininfo(state){
                return localStorage.getItem('logininfo');
              }
          },
          modules :{
            
          }
    }