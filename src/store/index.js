// Create a new store instance.
import { createStore } from 'vuex'
import Authstore from '../Authstore/index'

const store = createStore({
    state () {
      return {
      
      }
    },
  
    //mutations
    mutations: {
      
    
    },
  
     
   //actions
  actions :{
   
  
    },//end of actions
  
  
    getters :{
      
      
    },
    modules :{
        Authstore,
    
    }
  });
  export default store;
  