import {createRouter,createWebHashHistory} from 'vue-router'
import initialpage from '../pages/initialpage.vue'
import homepage from '../pages/HomePage.vue'
import LoginPage from '../components/LoginPage.vue'

const routes= [

          {
        path:'/',
        name:'initial',
       component:initialpage
          },
     
          {
            path:'/home',
            name:'home',
            meta:{requiresAuth:true},
            component:homepage,
           },
          
         {
            path:'/Login',
            name:'LoginPage',
            component:LoginPage
           },
           {
            path:'/home',
            name:'homewithoutauth',
            component:homepage,
           },
         
          
          
         
         
    
    ];

const router =createRouter({
  
    history: createWebHashHistory(),
    
    routes,
  });
  
router.beforeEach((to,from,next)=>{
  if(to.meta?.requiresAuth){
     next({name:'LoginPage'})
      }else{
         next();
  }
})


  export default router;
 